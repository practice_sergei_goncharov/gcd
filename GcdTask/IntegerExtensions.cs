﻿using System;

namespace GcdTask
{
#pragma warning disable
    public static class IntegerExtensions
    {
        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue]  by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int FindGcd(int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("Both numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException("One or both numbers are int.MinValue.");
            }

            // Take absolute values to ensure positive values for calculations
            int absA = Math.Abs(a);
            int absB = Math.Abs(b);

            // Euclidean algorithm to find GCD
            while (absB != 0)
            {
                int remainder = absA % absB;
                absA = absB;
                absB = remainder;
            }

            // GCD will be the absolute value of absA
            return Math.Abs(absA);
        }
    }

}
